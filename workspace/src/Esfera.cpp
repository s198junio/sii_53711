// Esfera.cpp: implementation of the Esfera class.
//
//////////////////////////////////////////////////////////////////////

#include "Esfera.h"
#include "glut.h"
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

Esfera::Esfera()
{
	radio=0.5f;
	velocidad.x=3;
	velocidad.y=3;
	azul=0;
	rojo=255;
	verde=255;
}

Esfera::~Esfera()
{

}



void Esfera::Dibuja()
{
	
	glColor3ub(255,255,0);
	glEnable(GL_LIGHTING);
	glPushMatrix();
	glTranslatef(centro.x,centro.y,0);
	glutSolidSphere(radio,15,15);
	glPopMatrix();
}

void Esfera::Mueve(float t)
{
	centro.x = centro.x + velocidad.x * t;//Ecuacion del mov simple
	centro.y = centro.y + velocidad.y * t;

	if(radio > 0.20)// Si el radio es mayor que 0.2 se resta 0.05 para que vaya reduciendo su tamaño
		radio = radio - 0.05 * t;

}


